// --------------------------------------------------
// popup.js - call everytime user click on extension.
// --------------------------------------------------

init().catch(e => console.error(e))

async function init() {
  // Load config
  const [config, projectsConfig, favoritesConfig, realTimeIssues] = await loadConfig()
  // init popup
  await initPopup(config, projectsConfig, favoritesConfig, realTimeIssues)
}

async function loadConfig() {
  const syncConfigPromise = new Promise((resolve, reject) => {
    chrome.storage.sync.get(['config', 'projectsConfig', 'favoritesConfig'], (storage) => {
      const config = storage.config || []
      const projectsConfig = storage.projectsConfig || []
      const favoritesConfig = storage.favoritesConfig || []
      resolve([config, projectsConfig, favoritesConfig])
    })
  })
  const localConfigPromise = new Promise((resolve, reject) => {
    chrome.storage.local.get(['realTimeIssues'], (storage) => {
      const realTimeIssues = storage.realTimeIssues || []
      resolve([realTimeIssues])
    })
  })
  const allConfigs = await Promise.all([syncConfigPromise, localConfigPromise])
  return allConfigs[0].concat(allConfigs[1])
}

async function initPopup(config, projectsConfig, favoritesConfig, realTimeIssues) {
  if (!projectsConfig.length && !favoritesConfig.length) {
    // TODO : use open page function
    closeAllPage()
    timerTrackerNoProjectPage.style.display = 'block'
    mountActions(config, projectsConfig, favoritesConfig, realTimeIssues)
    return
  }

  const dataFavorites = await getDataFavorites(config, favoritesConfig)
  const dataProjects = await getDataProjects(config, projectsConfig)
  const htmlProjects = await getHtmlProjects(dataFavorites, dataProjects, config, realTimeIssues)
  timerTrackerProjectsPageContent.innerHTML = htmlProjects
  mountActions(config, projectsConfig, favoritesConfig, realTimeIssues)

  // TODO : use open page function
  closeAllPage()
  timerTrackerProjectsPage.style.display = 'block'
}

async function restart() {
  openPage(timerTrackerLoadPage)
  await init()
}

// ----------------
// popup functions
// ----------------

function closeAllPage() {
  timerTrackerLoadPage.style.display = 'none'
  timerTrackerNoProjectPage.style.display = 'none'
  timerTrackerProjectForm.style.display = 'none'
  timerTrackerFavoritesPage.style.display = 'none'
  timerTrackerProjectsPage.style.display = 'none'
  timerTrackerLogTimePage.style.display = 'none'
  timerTrackerConfirmForm.style.display = 'none'
}

function getHtmlProjects(favoritesData, projectsData, config, realTimeIssues) {
  const favoritesHtml = getHtmlFavorites(favoritesData, config)
  const projectsHtml = projectsData.map((projectData) => {
    if (!projectData.name) {
      return `
        <div class="Project Project--error center p-8">
            <img class="timerTrackerProjectErrorDeleteConfig floatRight cursorPointer" src="img/trash.png" width="24px" height="24px" alt="Delete config" title="Delete config" data-project-id="${projectData.id}" />
            <span class="valignMiddle">Your not connected to Jira or <span class="bold">${projectData.id}</span> is not a valid id for project...</span>
        </div>
      `
    }

    return getHtmlProject(projectData, config, realTimeIssues)
  })
  const outputHtml = projectsHtml.length ? projectsHtml.reduce((final, content) => final ? final + content : content) : ""
  return favoritesHtml + outputHtml
}

function getHtmlProject(projectData, config, realTimeIssues) {
  return `
    <div class="Project Project--${projectData.id}">
      <div class="Project-header p-12">
        <img class="Project-logo valignMiddle" src="${projectData.iconUrl}" alt="Project : ${projectData.name}" width="24px" height="24px" />
        <h3 class="Project-name valignMiddle" title="${getProjectTitle(projectData, realTimeIssues)}">
          <span class="bold">${projectData.name}</span> [${projectData.id}]
        </h3>
        <div class="Project-actions">
          <input class="Project-setTimeInput" type="number" title="Time on project" value="${getDefaultTimeInDay()}" />
          ${getRealTimeHtml(projectData)}
          <img
            class="Project-splitTimeAction"
            src="img/timeSplit.png"
            width="24px"
            height="24px"
            alt="Split time"
            title="Split time"
            data-project-id="${projectData.id}"
          />
          <img
            class="Project-validTimeAction"
            src="img/validTime.png"
            width="24px"
            height="24px"
            alt="Valid time"
            title="Valid all time"
            data-project-id="${projectData.id}"
          />
          <img
            class="Project-configAction"
            src="img/options.png"
            width="24px"
            height="24px"
            alt="Project config"
            title="Project config"
            data-project-id="${projectData.id}"
          />
        </div>
      </div>
      <div class="Project-issues Project-issues--${projectData.id}">
        ${getHtmlIssues(projectData, config)}
      </div>
    </div>
  `
}

function getHtmlFavorites(favoritesData, config) {
  if ((!favoritesData.issues || !favoritesData.issues.length) && (!favoritesData.errorMessages)) {
    return ''
  }

  const favoritesId = 'Favorites'
  let contentFavoritesHtml = ''
  if (favoritesData.errorMessages && favoritesData.errorMessages.length) {
    contentFavoritesHtml = `
        <div class="Project--error center p-8">
            <div>${favoritesData.errorMessages[0]}</div>
            <div>You can delete favorites issues in configuration.</div>
        </div>`
  } else {
    contentFavoritesHtml = getHtmlIssues(favoritesData, config)
  }
  return `
    <div class="Project Project--${favoritesId}">
      <div class="Project-header p-12">
        <img class="Project-logo valignMiddle" src="/img/star.png" alt="Project : ${favoritesId}" width="24px" height="24px" />
        <h3 class="Project-name valignMiddle" title="${favoritesId}">
          <span class="bold">${favoritesId}</span>
        </h3>
        <div class="Project-actions">
          <input class="Project-setTimeInput" type="number" title="Time on project" value="${getDefaultTimeInDay()}" />
          <img
            class="Project-splitTimeAction"
            src="img/timeSplit.png"
            width="24px"
            height="24px"
            alt="Split time"
            title="Split time"
            data-project-id="${favoritesId}"
          />
          <img
            class="Project-validTimeAction"
            src="img/validTime.png"
            width="24px"
            height="24px"
            alt="Valid time"
            title="Valid all time"
            data-project-id="${favoritesId}"
          />
          <img
            class="Project-favoritesConfigAction"
            src="img/options.png"
            width="24px"
            height="24px"
            alt="Project config"
            title="Project config"
            data-project-id="${favoritesId}"
          />
        </div>
      </div>
      <div class="Project-issues Project-issues--${favoritesId}">
        ${contentFavoritesHtml}
      </div>
    </div>
  `
}

function getProjectTitle(projectData, realTimeIssues) {
  const nbTracked = projectData.issues.length
  const nbPhantom = !realTimeIssues ? 0 : realTimeIssues.filter(issue => {
    return issue.sourceProject === projectData.id && issue.endTrackedMoment
  }).length
  const nbTotal = nbTracked + nbPhantom
  return `${projectData.name} [${projectData.id}]\nTracked issues: ${nbTracked}\nPhantom issues: ${nbPhantom}\nTotal issues: ${nbTotal}`
}

function getRealTimeHtml(projectData) {
  const realTimeHtml = !projectData.realTime ? '' : `
    <img
      class="Project-realTimeAction"
      src="img/realTime.png"
      width="24px"
      height="24px"
      alt="Real time"
      title="Real time"
      data-project-id="${projectData.id}"
    />
  `
  return realTimeHtml
}

function getHtmlIssues(projectData, config) {
  const issuesData = projectData.issues.filter(issue => {
    return Boolean(issue)
  })

  if (!issuesData || !issuesData.length) {
    return `
        <div class="center p-8">
            <span>You don't have tracked issues in this project...</span>
        </div>
      `
  }

  const issuesHtml = issuesData.map((issuesData) => {
    return `
      <div class="Project-issue p-8 Project-issue--${issuesData.key}" data-issue-id="${issuesData.key}">
        <a class="Project-issueName" href="${config.JIRA_ISSUE_ONLINE}${issuesData.key}">${issuesData.key} - ${issuesData.fields.summary}</a>
        <input class="Project-issueTime Project-issueTime--${issuesData.key}" type="number" name="${issuesData.key}-timeValue" value="0" />
        <button class="Project-issueTimeSubmit" data-issue-id="${issuesData.key}" data-project-id="${projectData.id}"><img src="img/check.png" title="Log time" /></button>
      </div>
    `
  })
  const outputHtml = issuesHtml.reduce((final, content) => final ? final + content : content)
  return outputHtml
}

async function getDataFavorites(config, favoritesConfig) {
  if (!favoritesConfig || !favoritesConfig.length) {
    return {}
  }
  const favoritesIdString = favoritesConfig.join(', ')
  const jqlQuery = config.JIRA_ISSUES_JQL_QUERY
  const finalJqlQuery = jqlQuery.replace('{{issuesId}}', favoritesIdString)
  const encodedJqlQuery = encodeURIComponent(finalJqlQuery).split('%2B').join('+')
  const urlIssues = config.JIRA_SEARCH_URL + encodedJqlQuery
  const response = await fetch(urlIssues)
  const favoritesData = await response.json()
  return favoritesData
}

// TODO : refactor shared functions (in popup.js and background.js)
async function getDataProjects(config, projectsConfig) {
  let dataProjects = []
  for (const projectConfig of projectsConfig) {
    const dataProject = await getDataProject(config, projectConfig)
    dataProjects.push(dataProject)
  }
  return dataProjects
}

async function getDataProject(config, projectConfig) {
  // TODO : make 'promise all' of project and issue requests for performance
  const projectId = projectConfig.id
  const urlProject = config.JIRA_PROJECT_URL.replace('{{projectId}}', projectId)
  const response = await fetch(urlProject)
  const projectsData = await response.json()
  const projectData = projectsData.projects && projectsData.projects.length ? projectsData.projects[0] : undefined
  const projectIconUrl = projectData ? projectData.avatarUrls['48x48'] : undefined
  const projectName = projectData ? projectData.name : undefined

  const returnData = {
    id: projectConfig.id,
    realTime: projectConfig.realTime,
    name: projectName,
    iconUrl: projectIconUrl,
    issues: []
  }

  if (!projectConfig.trackingStatus.length) {
    return returnData
  }

  // TODO: check if REST API can support multiple status issue for performance
  const promiseIssuesOfStatus = projectConfig.trackingStatus.map(async statusIssue => {
    const onlyMyIssue = projectConfig.onlyMyIssue
    const jqlQuery = onlyMyIssue ? config.JIRA_ISSUES_USER_JQL_QUERY : config.JIRA_ISSUES_PROJECT_JQL_QUERY
    const finalJqlQuery = jqlQuery.replace('{{projectId}}', `'${projectId}'`).replace('{{statusIssue}}', `'${statusIssue}'`)
    const encodedJqlQuery = encodeURIComponent(finalJqlQuery).split('%2B').join('+')
    const urlIssues = config.JIRA_SEARCH_URL + encodedJqlQuery
    const response2 = await fetch(urlIssues)
    const issuesData = await response2.json()
    return issuesData.issues
  })
  const issuesOfStatus = await Promise.all(promiseIssuesOfStatus)
  returnData.issues = [].concat.apply([], issuesOfStatus)

  return returnData
}

function mountActions(config, projectsConfig, favoritesConfig, realTimeIssues) {
  const positionScroll = timerTrackerProjectsPage.scrollTop
  removeAllEventListener()

  timerTrackerNoProjectPageBtn.addEventListener('click', () => openFormProject())

  const logTimeButtons = document.querySelectorAll('.Project-issueTimeSubmit')
  logTimeButtons.forEach(logTimeButton => {
    logTimeButton.addEventListener('click', event => workTimeSubmit(event, projectsConfig))
  })

  const splitTimeButtons = document.querySelectorAll('.Project-splitTimeAction')
  splitTimeButtons.forEach(logTimeButton => {
    logTimeButton.addEventListener('click', event => autoSplitTime(event))
  })
  const realTimeButtons = document.querySelectorAll('.Project-realTimeAction')
  realTimeButtons.forEach(realTimeButton => {
    realTimeButton.addEventListener('click', event => autoRealTime(event, config, projectsConfig, realTimeIssues, favoritesConfig))
  })
  const validTimeButtons = document.querySelectorAll('.Project-validTimeAction')
  validTimeButtons.forEach(validTimeButton => {
    validTimeButton.addEventListener('click', event => validAllProjectTime(event, config, projectsConfig))
  })

  const projectErrorDeleteButtons = document.querySelectorAll('.timerTrackerProjectErrorDeleteConfig')
  projectErrorDeleteButtons.forEach(deleteButton => {
    deleteButton.addEventListener('click', () => confirmDeleteProjectConfig(config, projectsConfig, deleteButton))
  })

  const favoriteConfigButtons = document.querySelectorAll('.Project-favoritesConfigAction')
  favoriteConfigButtons.forEach(configButton => {
    configButton.addEventListener('click', () => openFormFavorites(favoritesConfig))
  })

  timerTrackerAddFavorite.addEventListener('click', () => openFormFavorites(favoritesConfig))

  timerTrackerFavoriteAddButton.addEventListener('click', () => addFavorite(favoritesConfig))

  timerTrackerFavoriteInput.addEventListener('keyup', (event) => favoriteInputAction(event))

  timerTrackerFavoritesSave.addEventListener('click', () => saveFavorites())

  timerTrackerFavoritesClos.addEventListener('click', () => openPage(timerTrackerProjectsPage))

  const projectConfigButtons = document.querySelectorAll('.Project-configAction')
  projectConfigButtons.forEach(configButton => {
    configButton.addEventListener('click', () => openFormProject(projectsConfig, configButton))
  })

  timerTrackerAddProjects.addEventListener('click', openFormProject)

  timerTrackerProjectSave.addEventListener('click', () => trySaveProjectConfig(config, projectsConfig))

  timerTrackerProjectClos.addEventListener('click', () => openPage(timerTrackerProjectsPage))

  timerTrackerProjectDelete.addEventListener('click', event => {
    confirmDeleteProjectConfig(config, projectsConfig, event.target)
  })

  timerTrackerChangeLogBtn.addEventListener('click', () => openPage(timerTrackerChangeLogPage))

  timerTrackerLogTimeCancel.addEventListener('click', () => openPage(timerTrackerProjectsPage))
  timerTrackerLogTimeSave.addEventListener('click', () => saveLogTime(config))

  timerTrackerConfirmFormNo.addEventListener('click', () => restart())

  const linkElements = document.querySelectorAll('a')
  linkElements.forEach(linkElements => {
    linkElements.addEventListener('click', () => { chrome.tabs.create({ url: linkElements.href }) })
  })

  timerTrackerProjectsPage.scrollTop = positionScroll
}


// ------------
// User actions
// ------------

async function workTimeSubmit(event, projectsConfig) {
  const sourceElement = event.path[1]
  const issueId = sourceElement.dataset.issueId
  const projectId = sourceElement.dataset.projectId
  const projectConfig = getProjectConfigById(projectsConfig, projectId)

  const inputRelatedElement = document.querySelector(`.Project-issueTime--${issueId}`)
  const inputIssueNameElement = document.querySelector(`.Project-issue--${issueId} .Project-issueName`)
  timerTrackerLogIssue.value = inputIssueNameElement.innerHTML
  timerTrackerLogIssue.dataset.issueId = issueId
  timerTrackerLogTime.value = inputRelatedElement.value
  timerTrackerLogText.value = projectConfig.defaultMessage ? projectConfig.defaultMessage : 'Auto time log'
  openPage(timerTrackerLogTimePage)
}

async function saveLogTime(config) {
  const issueId = timerTrackerLogIssue.dataset.issueId
  const time = timerTrackerLogTime.value
  const message = timerTrackerLogText.value

  sendLogTime(issueId, time, message, config)
  const inputRelatedElement = document.querySelector(`.Project-issueTime--${issueId}`)
  inputRelatedElement.value = 0

  openPage(timerTrackerProjectsPage)
}

async function sendLogTime(issueId, timeValue, message, config) {
  const timeSpentSeconds = Number(timeValue) * 3600

  const urlWorkLog = config.JIRA_WORK_LOG_URL.replace('{{issueId}}', issueId)
  const workLogBody = {
    comment: message ? message : 'Auto time log',
    started: `${new Date().toISOString().split('T')[0]}T00:00:00.000+0000`,
    timeSpentSeconds
  }
  const optionsWorkLog = {
    method: 'POST',
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(workLogBody)
  }

  // TODO : check if response confirm time log
  const response = await fetch(urlWorkLog, optionsWorkLog)
  //const workLogData = await response.json()
}

function autoSplitTime(event) {
  const sourceElement = event.target
  const projectId = sourceElement.dataset.projectId
  const setTimeElements = document.querySelector(`.Project--${projectId} .Project-setTimeInput`)
  const setTime = Number(setTimeElements.value)
  const timeToSplit = setTime > 0 ? setTime : getDefaultTimeInDay()
  const inputElements = document.querySelectorAll(`.Project--${projectId} .Project-issueTime`)
  const issuesNumber = inputElements.length

  if (!issuesNumber) {
    return
  }

  const splittedTime = Number((timeToSplit / issuesNumber).toFixed(2))
  const diffTime = Number(((-splittedTime * issuesNumber) + timeToSplit).toFixed(2))
  const fixedTime = parseFloat((splittedTime + diffTime).toFixed(2))

  inputElements.forEach((element, index, elements) => {
    if (index === elements.length - 1) {
      element.value = fixedTime
      return
    }
    element.value = splittedTime
  })
}

function autoRealTime(event, config, projectsConfig, realTimeIssues, favoritesConfig) {
  const sourceElement = event.target
  const projectId = sourceElement.dataset.projectId
  const setTimeElements = document.querySelector(`.Project--${projectId} .Project-setTimeInput`)
  const setTime = Number(setTimeElements.value)
  const timeToSplit = setTime > 0 ? setTime : getDefaultTimeInDay()
  const realTimeIssuesForProject = realTimeIssues.filter(issue => {
    return issue.sourceProject === projectId
  })
  const savedRealTimes = realTimeIssuesForProject.map(issue => {
    const trackedDate = new Date(issue.startTrackedMoment)
    return issue.endTrackedMoment ?
      new Date(issue.endTrackedMoment).getTime() - trackedDate.getTime() :
      new Date().getTime() - trackedDate.getTime()
  })
  if (!savedRealTimes.length) {
    return
  }
  const totalRealTime = savedRealTimes.reduce((total, time) => {
    return total + time
  })

  let totalLoggedTime = 0
  realTimeIssuesForProject.forEach((realTimeIssue, index, allRealTimeIssues) => {
    const trackedDate = new Date(realTimeIssue.startTrackedMoment)
    const diffTime = realTimeIssue.endTrackedMoment ?
      new Date(realTimeIssue.endTrackedMoment).getTime() - trackedDate.getTime() :
      new Date().getTime() - trackedDate.getTime()
    let valueTime = 0
    if(index === allRealTimeIssues.length - 1) {
      valueTime = Number((timeToSplit - totalLoggedTime).toFixed(2))
    } else {
      valueTime = Number(((diffTime / totalRealTime) * timeToSplit).toFixed(2))
      totalLoggedTime = totalLoggedTime + valueTime
    }

    const inputIssue = document.querySelector(`.Project-issueTime--${realTimeIssue.key}`)
    if (inputIssue) {
      inputIssue.value = valueTime
      inputIssue.setAttribute("value", valueTime.toString())
    } else {
      const htmlPhantomIssue = `
        <div class="Project-issue Project-issue--${realTimeIssue.key} Project-issue--phantom p-8" data-issue-id="${realTimeIssue.key}">
          <span class="Project-issueName">${realTimeIssue.key} - ${realTimeIssue.fields.summary}</span>
          <input class="Project-issueTime Project-issueTime--${realTimeIssue.key}" type="number" name="${realTimeIssue.key}-timeValue" value="${valueTime}" />
          <button class="Project-issueTimeSubmit" data-issue-id="${realTimeIssue.key}" data-project-id="${realTimeIssue.sourceProject}"><img src="img/check.png" title="Log time" /></button>
        </div>
      `
      const containerProjectIssues = document.querySelector(`.Project-issues--${projectId}`)
      containerProjectIssues.innerHTML += htmlPhantomIssue
    }
  })

  mountActions(config, projectsConfig, favoritesConfig, realTimeIssues)
}

function openFormFavorites(favoritesConfig = []) {
  timerTrackerFavoritesForm.innerHTML = favoritesConfig.map(favoriteId => {
    return getFavoriteRow(favoriteId)
  }).join('')

  mountFavoritesActions()

  openPage(timerTrackerFavoritesPage)
}

function getFavoriteRow(favoriteId) {
  return `<div class="Favorite flex-row mb-16 p-8" data-favorite-id="${favoriteId}">
    <span class="bold flex-item">${favoriteId}</span>
    <img class="Favorite-delete" data-favorite-id="${favoriteId}" src="img/trash.png" title="Delete" alt="Delete" />
  </div>`
}

function mountFavoritesActions() {
  const favoriteDeleteButtons = document.querySelectorAll('.Favorite-delete')
  favoriteDeleteButtons.forEach(configButton => {
    configButton.addEventListener('click', (event) => deleteFavorites(event))
  })
}

function addFavorite(favoritesConfig) {
  timerTrackerFavoriteMessage.innerHTML = ""
  const newFavoriteId = timerTrackerFavoriteInput.value

  if (newFavoriteId.includes(' ')) {
    timerTrackerFavoriteMessage.innerHTML = "<span class='primaryDark'>Issue id can't have space...</span>"
    return
  }

  if (favoritesConfig.includes(newFavoriteId)) {
    timerTrackerFavoriteMessage.innerHTML = `<span class='primaryDark'>${newFavoriteId} is already in your favorites.</span>`
    return
  }

  timerTrackerFavoriteInput.value = ''
  timerTrackerFavoritesForm.innerHTML = timerTrackerFavoritesForm.innerHTML + getFavoriteRow(newFavoriteId)
  mountFavoritesActions()
}

function favoriteInputAction(event) {
  if (event.keyCode === 13) {
    timerTrackerFavoriteAddButton.click()
  }
}

function deleteFavorites(event) {
  const favoriteId = event.target.dataset.favoriteId
  const favoriteElement = document.querySelector(`.Favorite[data-favorite-id="${favoriteId}"]`)
  favoriteElement.remove()
}

function saveFavorites() {
  const favoritesElement = document.querySelectorAll('.Favorite')
  const favoritesConfig = favoritesElement.length ? Array.from(favoritesElement).map(favoriteElement => favoriteElement.dataset.favoriteId) : []
  chrome.storage.sync.set({ favoritesConfig })
  restart()
}

function openFormProject(projectsConfig = {}, element = false) {
  closeAllPage()
  projectFormError.innerHTML = ""

  if(element) {
    const projectId = element.dataset.projectId
    const projectConfig = getProjectConfigById(projectsConfig, projectId)
    if (projectConfig) {
      projectFormId.value = projectConfig.id
      projectFormId.disabled = true
      projectFormTracking.value = projectConfig.trackingStatus.join(', ')
      timerTrackerProjectOnlyMyIssues.checked = projectConfig.onlyMyIssue
      timerTrackerProjectNotMyIssues.checked = !projectConfig.onlyMyIssue
      timerTrackerProjectRealTime.checked = projectConfig.realTime
      timerTrackerProjectNotRealTime.checked = !projectConfig.realTime
      timerTrackerProjectDefaultMessage.value = projectConfig.defaultMessage ? projectConfig.defaultMessage : ''
      timerTrackerProjectDelete.style.display = 'block'
      timerTrackerProjectDelete.setAttribute('data-project-id', projectConfig.id)
    }
  } else {
    projectFormId.value = ''
    projectFormId.disabled = false
    projectFormTracking.value = ''
    timerTrackerProjectOnlyMyIssues.checked = true
    timerTrackerProjectNotMyIssues.checked = false
    timerTrackerProjectRealTime.checked = false
    timerTrackerProjectNotRealTime.checked = true
    timerTrackerProjectDefaultMessage.value = ''
    timerTrackerProjectDelete.style.display = 'none'
    timerTrackerProjectDelete.setAttribute('data-project-id', '')
  }

  timerTrackerProjectForm.style.display = 'block'
}

function confirmDeleteProjectConfig(config, projectsConfig, element) {
  const title = 'Delete config project'
  const text = `You will delete the configuration for the <span class="bold">${element.dataset.projectId}</span> project. Are you sure ?`
  const action = () => {
    deleteProjectConfig(config, projectsConfig, element)
  }
  confirmAction(title, text, action)
}

function deleteProjectConfig(config, projectsConfig, element) {
  closeAllPage()
  timerTrackerLoadPage.style.display = 'block'
  projectsConfig = projectsConfig.filter(projectConfig => {
    return projectConfig.id !== element.dataset.projectId
  })
  chrome.storage.sync.set({ projectsConfig })
  restart()
}

async function trySaveProjectConfig(config, projectsConfig) {
  timerTrackerLoadPage.style.display = 'block'
  setTimeout(async () => {
    await saveProjectConfig(config, projectsConfig)
    timerTrackerLoadPage.style.display = 'none'
  }, 3000)
}

async function saveProjectConfig(config, projectsConfig) {
  projectFormError.innerHTML = ""

  const projectId = projectFormId.value
  if (projectId === "") {
    projectFormError.innerHTML = "Project id is required."
    return
  }

  const projectExist = await checkProjectExist(config, projectId)
  if (!projectExist) {
    projectFormError.innerHTML = `Project ${projectId} not found.`
    return
  }

  const projectTrackingString = projectFormTracking.value
  if (projectTrackingString === "") {
    projectFormError.innerHTML = "At least one tracked status id is required."
    return
  }
  const projectTrackingStatus = projectTrackingString.split(',').map(statusId => {
    return statusId.trim()
  })

  const projectOnlyMyIssues = !timerTrackerProjectNotMyIssues.checked
  const projectRealTime = timerTrackerProjectRealTime.checked
  const projectDefaultMessage = timerTrackerProjectDefaultMessage.value ? timerTrackerProjectDefaultMessage.value : undefined

  if (projectFormId.disabled) {
    projectsConfig = projectsConfig.map(projectConfig => {
      if (projectConfig.id === projectId) {
        projectConfig.trackingStatus = projectTrackingStatus
        projectConfig.onlyMyIssue = projectOnlyMyIssues
        projectConfig.realTime = projectRealTime
        projectConfig.defaultMessage = projectDefaultMessage
      }
      return projectConfig
    })
  } else {
    const projectAlreadyExist = projectsConfig.filter(projectConfig => {
      return projectConfig.id === projectId
    })

    if (projectAlreadyExist.length) {
      projectFormError.innerHTML = `${projectId} is already configured... Edit config if you want change value.`
      return
    }

    const newProjectData = {
      id: projectId,
      trackingStatus: projectTrackingStatus,
      onlyMyIssue: projectOnlyMyIssues,
      realTime: projectRealTime,
      defaultMessage: projectDefaultMessage
    }
    projectsConfig.push(newProjectData)
  }

  chrome.storage.sync.set({ projectsConfig })
  restart()
}

async function checkProjectExist(config, projectId) {
  const urlProject = config.JIRA_PROJECT_INFO_URL.replace('{{projectId}}', projectId)
  const response = await fetch(urlProject)
  const projectData = await response.json()
  return Boolean(projectData.id)
}

function validAllProjectTime(event, config, projectsConfig) {
  const sourceElement = event.target
  const projectId = sourceElement.dataset.projectId
  const projectConfig = getProjectConfigById(projectsConfig, projectId)
  const projectIssuesContainer = document.querySelector(`.Project-issues--${projectId}`)
  const projectIssuesElement = Array.from(projectIssuesContainer.children)

  projectIssuesElement.forEach(issueHtml => {
    const issueId = issueHtml.dataset.issueId
    const issueInput = issueHtml.querySelector('.Project-issueTime')
    if (issueInput && issueInput.value > 0) {
      sendLogTime(issueId, issueInput.value, projectConfig.defaultMessage, config)
      issueInput.value = 0
    }
  })
}


// ---------------
// Other functions
// ---------------

function getProjectConfigById(projectsConfig, projectId) {
  const projectMatch = projectsConfig.filter(projectConfig => {
    return projectConfig.id === projectId
  })
  return projectMatch.length ? projectMatch[0] : false
}

function openPage(pageObject) {
  closeAllPage()
  pageObject.style.display = 'block'
}

function confirmAction(title, text, action) {
  openPage(timerTrackerConfirmForm)
  timerTrackerConfirmFormTitle.innerHTML = title
  timerTrackerConfirmFormText.innerHTML = text
  timerTrackerConfirmFormYes.onclick = action
}

function removeAllEventListener() {
  const body = document.querySelector('body')
  const bodyClone = body.cloneNode(true)

  body.parentNode.replaceChild(bodyClone, body)
}

function getDefaultTimeInDay() {
  return new Date().getDay() === 5 ? 7 : 8
}