const JIRA_BASE_URL = "https://jira.kaliop.net/" // if edit also change in manifest
const JIRA_ISSUE_ONLINE = JIRA_BASE_URL + 'browse/'
const JIRA_ISSUES_PROJECT_JQL_QUERY = 'project={{projectId}}+AND+status={{statusIssue}}'
const JIRA_ISSUES_JQL_QUERY = 'issuekey in ({{issuesId}})'
const JIRA_ISSUES_URL = JIRA_BASE_URL + "rest/api/2/search?jql=project={{projectId}}+AND+status={{statusIssue}}"
const JIRA_ISSUES_USER_JQL_QUERY = 'assignee=currentuser()+AND+project={{projectId}}+AND+status={{statusIssue}}'
const JIRA_PROJECT_INFO_URL = JIRA_BASE_URL + "rest/api/2/project/{{projectId}}"
const JIRA_PROJECT_URL = JIRA_BASE_URL + "rest/api/2/issue/createmeta?projectKeys={{projectId}}"
const JIRA_SEARCH_URL = JIRA_BASE_URL + "rest/api/2/search?jql="
const JIRA_WORK_LOG_URL = JIRA_BASE_URL + "rest/api/2/issue/{{issueId}}/worklog"

init().catch(e => console.error(e))

async function init() {
  const config = {
    JIRA_ISSUE_ONLINE,
    JIRA_ISSUES_PROJECT_JQL_QUERY,
    JIRA_ISSUES_JQL_QUERY,
    JIRA_ISSUES_URL,
    JIRA_ISSUES_USER_JQL_QUERY,
    JIRA_PROJECT_INFO_URL,
    JIRA_PROJECT_URL,
    JIRA_SEARCH_URL,
    JIRA_WORK_LOG_URL
  }
  chrome.storage.sync.set({ config })

  // FOR DEV : init with projects configuration
  // const projectsConfig = [
  //   {
  //     id: 'BIOGENNEU',
  //     realTime: true,
  //     onlyMyIssue: true,
  //     trackingStatus: ['BN - IN PROGRESS', 'BN - BLOCKED', 'BN - IN REVIEW']
  //   },
  //   {
  //     id: 'KFIT2K18',
  //     trackingStatus: ['OPEN']
  //   }
  // ]
  // chrome.storage.sync.set({ projectsConfig })
  //const favoritesConfig = ['BIOGENNEU-915', 'KFIT2K18-8', 'BIOGENNEU-919']
  //chrome.storage.sync.set({ favoritesConfig })

  const intervalTime = 15 * 60 * 60 // all 15 min
  setInterval(() => backgroundTask(), intervalTime)
  await backgroundTask()
}

async function loadConfig() {
  const syncConfigPromise = new Promise((resolve, reject) => {
    chrome.storage.sync.get(['config', 'projectsConfig'], (storage) => {
      const config = storage.config || []
      const projectsConfig = storage.projectsConfig || []
      resolve([config, projectsConfig])
    })
  })
  const localConfigPromise = new Promise((resolve, reject) => {
    chrome.storage.local.get(['realTimeIssues'], (storage) => {
      const realTimeIssues = storage.realTimeIssues || []
      resolve([realTimeIssues])
    })
  })
  const allConfigs = await Promise.all([syncConfigPromise, localConfigPromise])
  return allConfigs[0].concat(allConfigs[1])
}

async function backgroundTask() {
  const [config, projectsConfig, loadedRealTimeIssues] = await loadConfig()
  let realTimeIssues = loadedRealTimeIssues ? loadedRealTimeIssues : []
  const projectsData = await getDataProjects(config, projectsConfig)
  if (!projectsData.length) {
    return
  }
  console.log('projectsData', projectsData)
  realTimeIssues = updateRealTimeIssues(projectsData, realTimeIssues)

  chrome.storage.local.set({ projectsData, realTimeIssues })
}

function updateRealTimeIssues(projectsData, realTimeIssues) {
  projectsData.forEach(projectData => {
    if (!projectData.realTime) {
      return
    }
    projectData.issues.forEach(realTimeIssue => {
      const realTimeIssueAlreadyTracked = realTimeIssues.filter((issue) => {
        return issue.key === realTimeIssue.key
      }).length
      if (!realTimeIssueAlreadyTracked) {
        realTimeIssue.startTrackedMoment = new Date().toString()
        realTimeIssue.sourceProject = projectData.id
        realTimeIssues.push(realTimeIssue)
      }
    })
  })

  realTimeIssues.forEach((realTimeIssue) => {
    if (realTimeIssue.endTrackedMoment) {
      return
    }

    const sourceProject = projectsData.filter(projectData => {
      return projectData.id === realTimeIssue.sourceProject
    })
    const arraysOfTrackedIssues = sourceProject.map(project => project.issues)
    const allTrackedIssues = [].concat.apply([], arraysOfTrackedIssues)
    const issuesId = allTrackedIssues.map(issue => issue.key)

    if (!issuesId.includes(realTimeIssue.key)) {
      realTimeIssue.endTrackedMoment = new Date().toString()
    }
  })

  const newRealTimeIssues = realTimeIssues.filter(realTimeIssue => {
    return new Date(realTimeIssue.startTrackedMoment).toDateString() === new Date().toDateString()
  })
  return newRealTimeIssues
}

// TODO : refactor shared functions (in popup.js and background.js)
async function getDataProjects(config, projectsConfig) {
  let dataProjects = []
  for (const projectConfig of projectsConfig) {
    const dataProject = await getDataProject(config, projectConfig)
    dataProjects.push(dataProject)
  }
  return dataProjects
}

async function getDataProject(config, projectConfig) {
  // TODO : make 'promise all' of project and issue requests for performance
  const projectId = projectConfig.id
  const urlProject = config.JIRA_PROJECT_URL.replace('{{projectId}}', projectId)
  const response = await fetch(urlProject)
  const projectsData = await response.json()
  const projectData = projectsData.projects && projectsData.projects.length ? projectsData.projects[0] : undefined
  const projectIconUrl = projectData ? projectData.avatarUrls['48x48'] : undefined
  const projectName = projectData ? projectData.name : undefined

  const returnData = {
    id: projectConfig.id,
    realTime: projectConfig.realTime,
    name: projectName,
    iconUrl: projectIconUrl,
    issues: []
  }

  if (!projectConfig.trackingStatus.length) {
    return returnData
  }

  // TODO: check if REST API can support multiple status issue for performance
  const promiseIssuesOfStatus = projectConfig.trackingStatus.map(async statusIssue => {
    const onlyMyIssue = projectConfig.onlyMyIssue
    const jqlQuery = onlyMyIssue ? config.JIRA_ISSUES_USER_JQL_QUERY : config.JIRA_ISSUES_JQL_QUERY
    const finalJqlQuery = jqlQuery.replace('{{projectId}}', `'${projectId}'`).replace('{{statusIssue}}', `'${statusIssue}'`)
    const encodedJqlQuery = encodeURIComponent(finalJqlQuery).split('%2B').join('+')
    const urlIssues = config.JIRA_SEARCH_URL + encodedJqlQuery
    const response2 = await fetch(urlIssues)
    const issuesData = await response2.json()
    return issuesData.issues
  })
  const issuesOfStatus = (await Promise.all(promiseIssuesOfStatus)).filter(issueOfStatus => {
    return Boolean(issueOfStatus)
  })
  returnData.issues = [].concat.apply([], issuesOfStatus)

  return returnData
}