# Kaliop - Jira Time Tracker
## Installation
- Télécharger le projet (extraire l'archive si .zip).
- Ouvrir Chrome et installer l'extension comme une source non empaquetée.
    - Accédez à chrome://extensions/.
    - En haut à droite de l'écran, activez le mode développeur.
    - Cliquez sur Chargez l'extension non empaquetée.
    - Recherchez et sélectionnez le dossier de l'application ou de l'extension.

## Configuration
- Vous devez être connecté à Jira Web pour que l'extension puissent se connecter.
- Cliquer sur "Add Project".
- Remplir l'id du projet que vous désirez connecter (pour trouver l'id simplement utiliser l'outil de recherche d'issues de Jira Web).
- Remplir les ids des statuts que vous désirez suivre (utiliser une nouvelle fois l'outil de recherche d'issues pour les trouver facilement).
- L'option "Only my issues" permet de récupérer seulement les issues qui vous sont assignées.
- L'option "Real Time" permet un suivit plus précis de votre activité sur le projet (activer cette option seulement s'il s'agit de votre projet principal. Des requêtes vont être effectuées toutes les 15min pour le suivi)
